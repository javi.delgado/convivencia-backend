from sqlobject import *


class Purse(SQLObject):
    money = FloatCol()
    user = ForeignKey("User", unique=True)
