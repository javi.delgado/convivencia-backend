from api_tornado.handlers import ApiHandler
from user.services import UserService


class UserHandler(ApiHandler):
    service = UserService

    def get(self):
        pass

    def post(self):
        service = self.get_service()
        response = service.create(self.request.body)
        self.write(response)

