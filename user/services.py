from api_tornado.services import BaseService
from user.models import User
from user.serializers import UserSerializer

class UserService(BaseService):
    model = User
    serializer = UserSerializer

    def create(self, args, **kwargs):
        serializer = self.serializer(args)
        if serializer.is_valid():
            return serializer.save()
        return serializer.get_validation()

    def profile(self):
        pass

    def update(self):
        pass

    def delete(self):
        pass