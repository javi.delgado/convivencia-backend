from tornado.web import Application
from user.handlers import UserHandler

urls = Application([
        (r"/", UserHandler),
    ])
