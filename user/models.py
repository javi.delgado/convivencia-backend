from sqlobject import *
from api_tornado.models import ModelBase


class User(ModelBase):

    class sqlmeta:
        table = "user_account"

    username = StringCol(length=15, unique=True)
    email = StringCol(length=25, unique=True)
    first_name = StringCol(length=25)
    last_name = StringCol(length=25)
    phone = StringCol(length=9)
    homework = ForeignKey("Homework")
    password = StringCol(length=25)


class Sesion(ModelBase):
    user = ForeignKey("User")
    sesion = StringCol(unique=True)

