from api_tornado.serializers import BaseSerializer
from user.models import User
from homework.models import Homework
from incoming import datatypes


class UserSerializer(BaseSerializer):
    model = User

    username = datatypes.String()
    email = datatypes.String()
    first_name = datatypes.String()
    last_name = datatypes.String()
    phone = datatypes.String()
    password = datatypes.String()
    homework = datatypes.Function('validate_homework',
                                         error="Homework ID don't exist")
    repeat_password = datatypes.Function('validate_password',
                                         error="Las contraseñas no coinciden")

    def validate_password(self, val, **kwargs):
        payload = kwargs['payload']
        return payload["password"] == val

    def validate_homework(self, val, **kwargs):
        return val is None or Homework.get(id=val)

