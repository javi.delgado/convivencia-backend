from tornado.web import RequestHandler
from tornado.escape import json_decode, json_encode

import json


class ApiHandler(RequestHandler):
    METHODS = RequestHandler.SUPPORTED_METHODS
    service = None
    
    # def initialize(self):
        # self.serializador(data=data)


    def prepare(self):
        self.request.body = json_decode(self.request.body)

    def write(self, args):
        args = json_encode(args)
        super(ApiHandler, self).write(args)

    def get_service(self):
        return self.service()
