from sqlobject.sqlbuilder import Insert
from tornado.escape import json_decode, json_encode

class BaseService:
    model = None
    serializer = None

    def create(self, args, **kwargs):
        pass

    def get(self):
        pass

    def update(self):
        pass

    def delete(self):
        pass


