from incoming import PayloadValidator

class BaseSerializer(PayloadValidator):
    model = None
    required_error = "Falta un valor para este campo."
    payload = {}

    def __init__(self, data=None, required=None, strict=None):
        super(BaseSerializer, self).__init__()
        self.data = data
        self.validate(self.data, required,strict)

    def validate(self, payload, required=None, strict=None):
        response = super(BaseSerializer, self).validate(payload, required, strict)
        self.valid = response[0]
        self.errors = response[1]

    def is_valid(self):
        return self.valid

    def get_validation(self):
        return {"result": self.valid, "errors": self.errors}

    def save(self):
        model = self.model(**self.data)
        return model

    def update(self):
        pass

    def get_model(self):
        return
