from sqlobject import *

######## database ##########

PSQL_CONF = {
    "host":"localhost",
    "port": "5432",
    "user": "postgres",
    "password": "postgres",
    "database": "convivencia"
}

CONEXION = "postgres://{user}:{password}@{host}:{port}/{database}".format(**PSQL_CONF)


# sqlhub.processConnection = connectionForURI(CONEXION)
