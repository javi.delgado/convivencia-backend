# from setting.site_urls import URL_PATTERN
from sqlobject import sqlhub, connectionForURI

from setting.database import CONEXION
from user.urls import urls as user_urls

from tornado.ioloop import IOLoop
from tornado.web import Application


def make_app():
    return user_urls


if __name__ == "__main__":
    app = make_app()
    app.listen(8000)
    sqlhub.processConnection = connectionForURI(CONEXION)
    IOLoop.current().start()
