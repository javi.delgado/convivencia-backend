from sqlobject import *


class ShoppingList(SQLObject):
    name = StringCol(length=25)


class Product(SQLObject):
    shopping_list = ForeignKey("ShoppingList", unique=True)
    name = StringCol(length=25)
    bought = BoolCol(default=False)
    price = FloatCol()
