from sqlobject import *


class Debts(SQLObject):
    name = StringCol(length=25)
    paid = BoolCol(default=False)
    price = FloatCol()
    recurrent = BoolCol(default=False)
