from sqlobject import *


class Month(SQLObject):
    start = DateCol()
    end = DateCol()
